let username;
let password;
let role;

function login(){
	username = prompt("Enter your username:").toLowerCase();
	password = prompt("Enter your password:").toLowerCase();
	role = prompt("Enter your role:").toLowerCase();

	if (username == 0 || password == 0 || role == 0){
		alert("Input should not be empty");
	}
	else{
		switch (role){
			case 'admin':
				alert("Welcome back to the class portal, admin!");
				break;
			case 'teacher':
				alert("Thankk you for logging in, teacher!");
				break;
			case 'student':
				alert("Welcome to the class portal, student!");
				break;

			default:
				alert("Role out of range!");
		}
	}
}

login();

function calculateAverage(num1, num2, num3, num4){
	let average = Math.round((num1 + num2 + num3 + num4) /4);


	try{
		if(average<75){
			console.log("Hello,student. Your average is " + average + " The letter equivalent is F.");
		}
		else if(average<=79){
			console.log("Hello,student. Your average is " + average + " The letter equivalent is D.");
		}
		else if(average<=84){
			console.log("Hello,student. Your average is " + average + " The letter equivalent is C.");
		}
		else if(average<=89){
			console.log("Hello,student. Your average is " + average + " The letter equivalent is B.");
		}
		else if(average<=95){
			console.log("Hello,student. Your average is " + average + " The letter equivalent is A.");
		}
		else if(average>96){
			console.log("Hello,student. Your average is " + average + "The letter equivalent is A+.");
		}
		else{
			console.log("Invalid average!");
		}
	}
	catch(error){
		console.warn(error);
	}
	finally{
		console.log("Use calculateAverage() to compute another average.");
	}
}

console.log("This is a sample set of 4 numbers: 95, 92, 80, 75");
calculateAverage(95, 92, 80, 75);